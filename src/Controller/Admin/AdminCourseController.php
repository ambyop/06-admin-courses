<?php

namespace App\Controller\Admin;

use App\Entity\Course;
use App\Form\CourseEditType;
use App\Form\CourseType;
use App\Repository\CourseRepository;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManagerInterface;
use phpDocumentor\Reflection\Types\This;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminCourseController extends AbstractController
{
    /**
     * @Route("/admin/course", name="admin_course")
     * @param CourseRepository $repository
     * @return Response
     */
    public function courses(CourseRepository $repository): Response
    {
        return $this->render('admin/admin-course.html.twig',
            [
                'courses' => $repository->findAll()
            ]);
    }

    /**
     * @Route("/admin/viewcourse/{id}", name="admin_course_view")
     * @param Course $course
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function viewCourse(Course $course, EntityManagerInterface $manager): Response
    {
        $course->setIsPublished(!$course->getIsPublished());
        $manager->flush();
        return $this->redirectToRoute('admin_course');
    }

    /** @Route("/admin/delcourse/{id}", name="admin_course_del")
     * @param Course $course
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function delCourse(Course $course, EntityManagerInterface $manager): Response
    {
        $manager->remove($course);
        $manager->flush();
        $this->addFlash('success', 'Le cours ' . $course->getName() . ' a bien été supprimé');
        return $this->redirectToRoute('admin_course');
    }

    /**
     * @Route("/admin/addcourse", name="admin_course_add")
     * @param EntityManagerInterface $manager
     * @param Request $request
     * @return Response
     */
    public function addCourse(EntityManagerInterface $manager, Request $request): Response
    {
        $course = new Course();
        $form = $this->createForm(CourseType::class, $course);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $slugify = new Slugify();
            $course->setCreatedAt(new \DateTime('now', new \DateTimeZone('Europe/Brussels')))
                ->setSlug($slugify->slugify($course->getName()));
            $manager->persist($course);
            $manager->flush();
            $this->addFlash('success', 'Le cours ' . $course->getName() . ' a bien été créé.');

            return $this->redirectToRoute('admin_course');
        }

        return $this->render("admin/add-course.html.twig", [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/editcourse/{id}", name="admin_course_edit")
     * @param Course $course
     * @param EntityManagerInterface $manager
     * @param Request $request
     * @param CourseRepository $repository
     */
    public function editCourse(Course $course, EntityManagerInterface $manager, Request $request, CourseRepository $repository)
    {
        $form = $this->createForm(CourseEditType::class, $course);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $slugify = new Slugify();
            $course->setSlug($slugify->slugify($course->getName()));
            $manager->persist($course);
            $manager->flush();
            $this->addFlash('success', 'Le cours ' . $course->getName() . ' a bien été edité');
            return $this->redirectToRoute('admin_course');
        }
        return $this->render('admin/edit-course.html.twig', [
            'form' => $form->createView(),
            'course' => $course
        ]);
    }
}