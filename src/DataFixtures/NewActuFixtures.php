<?php

namespace App\DataFixtures;

use App\Entity\NewActu;
use Cocur\Slugify\Slugify;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class NewActuFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();
        $slugify = new Slugify();
        for ($i = 1 ; $i <= 20; $i++){
            $newActu = new NewActu();
            $newActu->setName($faker->sentence(2,true));
            $newActu->setContent($faker->paragraphs(1,true));
            $newActu->setCreatedAt($faker->dateTimeThisYear('now'));
            $newActu->setSlug($slugify->slugify($newActu->getName()));
            $newActu->setImage('0'.$i.'.png');
            $manager->persist($newActu);
        }
        $manager->flush();
    }
}
