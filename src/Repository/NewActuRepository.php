<?php

namespace App\Repository;

use App\Entity\NewActu;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method NewActu|null find($id, $lockMode = null, $lockVersion = null)
 * @method NewActu|null findOneBy(array $criteria, array $orderBy = null)
 * @method NewActu[]    findAll()
 * @method NewActu[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NewActuRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NewActu::class);
    }

    // /**
    //  * @return NewActu[] Returns an array of NewActu objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?NewActu
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
